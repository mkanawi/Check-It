steps = [
    [
        # Create the table
        """
        CREATE TABLE claims (
            id SERIAL PRIMARY KEY NOT NULL,
            text VARCHAR(1000) NOT NULL,
            claimant VARCHAR(1000) NOT NULL,
            date VARCHAR(1000) NOT NULL,
            textualRating VARCHAR(1000) NOT NULL,
            Publisher VARCHAR(1000) NOT NULL,
            url VARCHAR(1000) NOT NULL,
            category_name VARCHAR(1000) NOT NULL,
            user_id INTEGER NOT NULL
                    REFERENCES accounts("id") ON DELETE CASCADE,

            CONSTRAINT fk_claims_categories
                FOREIGN KEY (category_name, user_id)
                REFERENCES categories (category_name, user_id)
                ON DELETE CASCADE
        );
        """,
        # Drop the table
        """
        DROP TABLE claims;
        """,
    ]
]
