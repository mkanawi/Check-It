steps = [
    [
        # Create the table
        """
        CREATE TABLE accounts (
            id SERIAL PRIMARY KEY NOT NULL,
            hashed_password TEXT NOT NULL,
            first_name VARCHAR(200) NOT NULL,
            last_name VARCHAR(200) NOT NULL,
            username VARCHAR(200) NOT NULL UNIQUE
        );
        """,
        # Drop the table
        """
        DROP TABLE accounts;
        """,
    ]
]
