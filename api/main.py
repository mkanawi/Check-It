from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import os

# from api.routers import newClaims
from routers import claims, categories, accounts, newClaims
from authenticator import authenticator

app = FastAPI()
app.include_router(claims.router, tags=["Claims"])
app.include_router(newClaims.router, tags=["New claims"])
app.include_router(categories.router, tags=["Categories"])
app.include_router(accounts.router, tags=["Accounts"])
app.include_router(authenticator.router, tags=["Accounts"])


origins = [
    "http://localhost:3000",
    os.environ.get("CORS_HOST", None),
]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
def home():
    return "it works"
