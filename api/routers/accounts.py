from fastapi import (
    Depends,
    HTTPException,
    status,
    Response,
    APIRouter,
    Request,
)
from authenticator import authenticator
from models import (
    AccountIn,
    AccountOut,
    AccountForm,
    AccountToken,
    HttpError,
)
from queries.accounts import AccountRepository, DuplicateAccountError


router = APIRouter()


@router.get("/token", response_model=AccountToken | None)
async def get_token(
    request: Request,
    account: AccountOut = Depends(authenticator.try_get_current_account_data),
) -> AccountToken | None:
    if account and authenticator.cookie_name in request.cookies:
        return {
            "access_token": request.cookies[authenticator.cookie_name],
            "type": "Bearer",
            "account": account,
        }


@router.post("/api/accounts", response_model=AccountToken | HttpError)
async def create_account(
    account_in: AccountIn,
    request: Request,
    response: Response,
    repo: AccountRepository = Depends(),
):
    hashed_password = authenticator.hash_password(account_in.password)
    try:
        account = repo.create(account_in, hashed_password)
    except DuplicateAccountError:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot create an account with those credentials",
        )
    form = AccountForm(
        first_name=account_in.first_name,
        last_name=account_in.last_name,
        username=account_in.username,
        password=account_in.password,
    )
    token = await authenticator.login(response, request, form, repo)
    return AccountToken(account=account, **token.dict())


@router.get("/api/user/{id}")
async def get_user(
    id: int,
    accounts: AccountRepository = Depends(),
    ra=Depends(authenticator.get_current_account_data),
) -> AccountOut:
    account = accounts.get_user_by_id(id)
    if not account:
        raise HTTPException(
            status.HTTP_400_BAD_REQUEST, detail="Account does not exist"
        )
    else:
        return account
