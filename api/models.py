from pydantic import BaseModel
from jwtdown_fastapi.authentication import Token


class SuccessMessage(BaseModel):
    success: bool


class Error(BaseModel):
    message: str


# CATEGORIES MODELS
class CategoryIn(BaseModel):
    category_name: str


class CategoryOut(BaseModel):
    id: int
    category_name: str


class UpdateCategory(BaseModel):
    name: str


# SAVED CLAIMS MODELS
class ClaimOut(BaseModel):
    id: int
    text: str
    claimant: str
    date: str
    textualRating: str
    publisher: str
    url: str
    category_name: str
    user_id: int


class ClaimIn(BaseModel):
    text: str
    claimant: str
    date: str
    textualRating: str
    publisher: str
    url: str
    category_name: str


class UpdateClaim(BaseModel):
    category_name: str


# AUTH MODELS
class AccountOut(BaseModel):
    id: int
    first_name: str
    last_name: str
    username: str


class AccountIn(BaseModel):
    first_name: str
    last_name: str
    username: str
    password: str


class AccountForm(BaseModel):
    first_name: str
    last_name: str
    username: str
    password: str


class AccountOutWithHashedPassword(AccountOut):
    hashed_password: str


class AccountToken(Token):
    account: AccountOut


class HttpError(BaseModel):
    detail: str
