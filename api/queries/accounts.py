from queries.pool import pool
from models import AccountIn, AccountOutWithHashedPassword, AccountOut


class DuplicateAccountError(ValueError):
    pass


class AccountRepository:
    def create(
        self, account_in: AccountIn, hashed_password: str
    ) -> AccountOutWithHashedPassword:
        try:
            # connect the database
            with pool.connection() as conn:
                # get a cursor (something to run SQL with)
                with conn.cursor() as db:
                    # Run our INSERT statement
                    result = db.execute(
                        """
                        INSERT INTO accounts
                            (hashed_password, first_name, last_name, username)
                        VALUEs
                            (%s, %s, %s, %s)
                        RETURNING id;
                        """,
                        [
                            hashed_password,
                            account_in.first_name,
                            account_in.last_name,
                            account_in.username,
                        ],
                    )
                    id = result.fetchone()[0]
                    old_data = account_in.dict()
                    old_data["hashed_password"] = hashed_password
                    del old_data["password"]
                    return AccountOutWithHashedPassword(id=id, **old_data)
        except Exception as e:
            print(e)
            return {"message": "Create did not work"}

    def get(self, username: str) -> AccountOutWithHashedPassword:
        # connect the database
        with pool.connection() as conn:
            # get a cursor (something to run SQL with)
            with conn.cursor() as db:
                # Run our INSERT statement
                result = db.execute(
                    """
                    SELECT id, hashed_password, first_name, last_name, username
                    FROM accounts
                    WHERE username = %s;
                    """,
                    [username],
                )
                account = result.fetchone()
                if account is None:
                    raise Exception("Could not find account")
                else:
                    try:
                        return AccountOutWithHashedPassword(
                            id=account[0],
                            hashed_password=account[1],
                            first_name=account[2],
                            last_name=account[3],
                            username=account[4],
                        )
                    except Exception as e:
                        raise Exception("error:", e)

    def get_account_by_id(self, id: int) -> AccountOutWithHashedPassword:
        # connect the database
        with pool.connection() as conn:
            # get a cursor (something to run SQL with)
            with conn.cursor() as db:
                # Run our INSERT statement
                result = db.execute(
                    """
                    SELECT id, hashed_password, first_name, last_name, username
                    FROM accounts
                    WHERE id = %s;
                    """,
                    [id],
                )
                account = result.fetchone()[0]
                if account is None:
                    raise Exception("Could not find account")
                else:
                    try:
                        return AccountOutWithHashedPassword(
                            id=account[0],
                            hashed_password=account[1],
                            first_name=account[2],
                            last_name=account[3],
                            username=account[4],
                        )
                    except Exception as e:
                        raise Exception("error:", e)

    def delete_user(self, id: int) -> AccountOut:
        # connect the database
        with pool.connection() as conn:
            # get a cursor (something to run SQL with)
            with conn.cursor() as db:
                db.execute(
                    """
                    DELETE FROM accounts
                    WHERE id = %s
                    RETURNING *;
                    """,
                    [id],
                )
