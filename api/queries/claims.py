import requests
from typing import List, Union
from queries.pool import pool
from models import ClaimIn, ClaimOut, Error, SuccessMessage, UpdateClaim
import os

SECRET_API_KEY = os.environ["SECRET_API_KEY"]


class ThirdPartyClaimRespository:
    def get_all_claims(self, query: str):
        params = {"query": query}
        res = requests.get(
            (
                "https://factchecktools.googleapis.com/"
                f"v1alpha1/claims:search?key={SECRET_API_KEY}"
                "&languageCode=en-US"
            ),
            params=params,
        )
        data = res.json()
        return data


class ClaimsRepository:
    def create(self, claim: ClaimIn, user_id: int) -> Union[ClaimOut, Error]:
        try:
            # connect the database
            with pool.connection() as conn:
                # get a cursor (something to run SQL with)
                with conn.cursor() as db:
                    # Run our INSERT statement
                    result = db.execute(
                        """
                        INSERT INTO claims
                            (text
                            , claimant
                            , date
                            , textualRating
                            , Publisher
                            , url
                            , category_name
                            , user_id)
                        VALUES
                            (%s, %s, %s, %s, %s, %s, %s, %s)
                        RETURNING id;
                        """,
                        [
                            claim.text,
                            claim.claimant,
                            claim.date,
                            claim.textualRating,
                            claim.publisher,
                            claim.url,
                            claim.category_name,
                            user_id,
                        ],
                    )
                    id = result.fetchone()[0]
                    return self.claims_in_to_out(id, claim, user_id)
        except Exception as e:
            print(e)
            return {"message": "Create did not work"}

    def get_all(self, user_id: int) -> Union[Error, List[ClaimOut]]:
        try:
            # connect the database
            with pool.connection() as conn:
                # get a cursor (something to run SQL with)
                with conn.cursor() as db:
                    # Run our SELECT statement
                    result = db.execute(
                        """
                        SELECT id
                                , text
                                , claimant
                                , date
                                , textualRating
                                , Publisher
                                , url
                                , category_name
                        FROM claims
                        WHERE user_id=%s
                        ORDER BY date;
                        """,
                        [user_id],
                    )
                    return [
                        self.record_to_claims_out(record, user_id)
                        for record in result
                    ]
        except Exception as e:
            print(e)
            return {"message": "Could not get all claims"}

    def claims_in_to_out(self, id: int, claim: ClaimIn, user_id: int):
        old_data = claim.dict()
        return ClaimOut(id=id, **old_data, user_id=user_id)

    def record_to_claims_out(self, record, user_id):
        return ClaimOut(
            id=record[0],
            text=record[1],
            claimant=record[2],
            date=record[3],
            textualRating=record[4],
            publisher=record[5],
            url=record[6],
            category_name=record[7],
            user_id=user_id,
        )

    def delete(self, save_id: int, user_id: int) -> SuccessMessage:
        try:
            # connect to the database
            with pool.connection() as conn:
                # get a cursor (something to run SQL with)
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM claims
                        WHERE (id = %s AND user_id = %s)
                        """,
                        [save_id, user_id],
                    )
                    if db.rowcount == 0:
                        return {"success": False}
                    else:
                        return {"success": True}
        except Exception as e:
            print(e)
            return {"success": False}

    def update(
        self, save_id: int, category_name: UpdateClaim, user_id: int
    ) -> SuccessMessage:
        try:
            # connect the database
            with pool.connection() as conn:
                # get a cursor (something to run SQL with)
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE claims
                        SET category_name = %s
                        WHERE (id = %s AND user_id=%s)
                        """,
                        [
                            category_name.category_name,
                            save_id,
                            user_id,
                        ],
                    )
                    if db.rowcount > 0:
                        return {"success": True}
                    else:
                        return {"success": False}
        except Exception as e:
            print(e)
            return {"success": False}
