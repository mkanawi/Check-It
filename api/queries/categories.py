from typing import List, Union
from queries.pool import pool
from models import (
    Error,
    CategoryIn,
    CategoryOut,
    SuccessMessage,
    UpdateCategory,
)


class CategoriesRepository:
    def create(
        self, category: CategoryIn, user_id: int
    ) -> Union[CategoryOut, Error]:
        try:
            # connect the database
            with pool.connection() as conn:
                # get a cursor (something to run SQL with)
                with conn.cursor() as db:
                    # Run our INSERT statement
                    result = db.execute(
                        """
                        INSERT INTO categories
                            (category_name, user_id)
                        VALUEs
                            (%s, %s)
                        RETURNING id;
                        """,
                        [category.category_name, user_id],
                    )
                    id = result.fetchone()[0]
                    return self.category_in_to_out(id, category, user_id)
        except Exception as e:
            print(e)
            return {"message": "Create did not work"}

    def get_all(self, user_id: int) -> Union[Error, List[CategoryOut]]:
        try:
            # connect the database
            with pool.connection() as conn:
                # get a cursor (something to run SQL with)
                with conn.cursor() as db:
                    # Run our SELECT statement
                    result = db.execute(
                        """
                        SELECT id, category_name
                        FROM categories
                        WHERE user_id=%s
                        ORDER BY id;
                        """,
                        [user_id],
                    )
                    return [
                        self.record_to_category_out(record, user_id)
                        for record in result
                    ]
        except Exception as e:
            print(e)
            return {"message": "Could not get all categories"}

    def category_in_to_out(self, id: int, category: CategoryIn, user_id: int):
        old_data = category.dict()
        return CategoryOut(id=id, **old_data, user_id=user_id)

    def record_to_category_out(self, record, user_id):
        return CategoryOut(
            id=record[0],
            category_name=record[1],
            user_id=user_id,
        )

    def delete(self, category_name: str, user_id: int) -> SuccessMessage:
        try:
            # connect the database
            with pool.connection() as conn:
                # get a cursor (something to run SQL with)
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM categories
                        WHERE (category_name = %s AND user_id=%s)
                        """,
                        [category_name, user_id],
                    )
                    # id = result.fetchone(category_name)
                    # result = db.fetchone()[0]
                    if db.rowcount == 0:
                        return {"success": False}
                    else:
                        return {"success": True}
        except Exception as e:
            print(e)
            return {"success": False}

    def update(
        self, category_id: int, name: UpdateCategory, user_id: int
    ) -> SuccessMessage:
        try:
            # connect the database
            with pool.connection() as conn:
                # get a cursor (something to run SQL with)
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE categories
                        SET category_name = %s
                        WHERE (id = %s AND user_id=%s)
                        """,
                        [
                            name.name,
                            category_id,
                            user_id,
                        ],
                    )
                    if db.rowcount > 0:
                        return {"success": True}
                    else:
                        return {"success": False}

                # look for result of db.execute db fetch one if is none
        except Exception as e:
            print(e)
            return {"success": False}
