## May 15, 2023
- As a group with Gabby writing the actual code, we coded tables, routers, queries, secret key. Helping debug any errors that occurred and gave input when needed.
- POST, GET category endpoints and POST, GET claims and GET(from 3rd part API) endpoints all work .
- We also hid our API key in .env file for security

## May 16, 2023
- As a group with Liam coding, we coded the auth routers, queries and authenticator.py file. Judith and I helped debug and gave input/thoughts.
- Gabby and Mo joined later on, we ended the day with almost finishing all authorization endpoints.

## May 17, 2023
- Gabby figured out what was wrong with our authentication endpoint and now they all work
- Gabby and I worked on creating a saved claim and get all saved claims for a specific user

## May 18, 2023
- Judith wrote the code, while we all trouble-shooted the delete and put endpoints for claims
- Mo wrote the code, while we all helped with the delete and put endpoints for categories

## May 22, 2023
- Gabby wrote the code for the frontend search bar as we all helped code and troubleshoot
- Fixed my react container with Gina and Lauren's help, now all containers work and I have functioning code

## May 23, 2023
- I was the one typing the code for the frontend authentication for our signup and login page, which all work now. Gabby and Judith helped, along with lots of SEIRs and Riley.
- Categories is authentication protected and only show the categories for the specific user logged in

## May 24, 2023
- Judith wrote the code for the logout button while Gabby and I helped/guided her. We hid it so you can only see it while logged in.
- Gabby wrote the code for saving a claim with the category dropdown while Judith and I helped. This option only shows if you are logged in, otherwise a login/create account is shown.
- Gabby wrote the code for the create new category feature for saving a claim with Judith and I helping.

## May 25, 202
- I wrote the code for a user to create their own categories with Liam, Gabby, Judith and Mo's help.
- I wrote the code for the dropdown on the saved claims page to filter through claims based on the category selected with the group helping.
- Mo updated the code so the recently saved disappears when you search a claim

## May 26, 2023
- I missed the morning but when I was there in the afternoon they added the update/delete category buttons. I believe Liam coded that with Gabby and Judith's help.
- We all picked a unit test to write and began working on that.
- I am slowly going through our files and cleaning up some of the code.

## May 30, 2023
- Judith and I worked on creating the 404 Error Page, I coded while she helped. We also fixed her unit test so that it passed.
- Worked on error handling for the backend, Gabby was the main coder while Mo, Judith and I helped.

## May 31, 2023
- Liam finished our last error handling for a put method.
- Gabby added 404 errors to the frontend.
- Liam, Judith and I helped Gabby fix her unit tests so they would all pass and there are no errors.

## June 1, 2023
- Gabby wrote the code as we all helped creating a light and dark mode for our website.
- I wrote some code to display a message if the search results in no claims.

## June 2, 2023
- We did some stretch goals, Liam changed our light and dark mode button to a sun and moon.
- Gabby added a magnifying icon for when searching for claims is loading, along we adding Judith's logo to our navbar.
- Judith and I cleaned up our code, got rid of files we no longer need and fixed any errors that occurred. (Shoutout Ryan and Garrett for helping us with that!)

## June 5, 2023
- Started deployment, working on passing the CI/CD tests and building an image so we can move onto Cirrus.

## June 6, 2023
- Still working on deployment, got all the CI/CD tests to pass and built an image
- Waiting to figure out Cirrus
