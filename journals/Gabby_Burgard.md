## Journals

Made tables, routers, queries, secret key all work so that we can save claims and categories through post and see them with get. Also made
endpoint for getting claim from gogoleAPI with secret key in .env file.

Had trouble with our requests not being the right version as well as struggled a bit getting the secret api key to import. Got
it all figure out though.

May 17
-fixed authentication code, got logout and signin working. Worked on creating saved claims for users and getting them all.

may 18-
-judith wrote the logout code and we fixed the delete and put endpoints for saved claims.

5/22
Made front end for search page, tried redux, decided it was simpler without given the simplicity of our project and states. Need to get auth working to test code weve written for categories dropdown in order to be abel to save claims.

5/23
Janelle did an awesome job of driving while we figured out how to make auth work for login and logout. I seperated the front end react compnents into more managable chunks (one compnent aprox per file), also added some logic so categories only show up when people are logged in.

5/24
Judith did a great job with the logout today. We got alot done, we fixed the save button mostly, still some work to do with gettin git to post the category and post the claim without having to press it twice. Mo fixed the login so it redirects to homepage. I made it so the recently saved claims show up when a person is logged in on their home page, I also made a Your Claims page with all of the users saved claims.

5/26
Liam wrote the buttons for the categories today, did a fantastic job. I worked on the first unit test for the third party api, took a bit, but got it working. I also started working on getting the save button to send s little "saved" message to user when they save a claim.

5/30
worked on handling errors and protexcted for empty data when saving a claim. Others worked ion 404 page and refreshing data when delted or cat changed.

may 31

- added 404 errors, fixed unit tests so they were all passing.

6/1
Today I implemented the lightmode button with the help of my group. That completed the official project with just some stratch goals left each of use wanted to try to implement before deployment. I also implemented a loading spinning magnifying glass as my stratch goal.

6/2

- added spinning magnifying glass on load and added logo to the nav, liam chenged dark mode button image, and jannelle cleaned up code getting ready for deployment next week.
