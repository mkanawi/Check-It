# APIs

## Saves Endpoints
![Saves Endpoints](./images/saves.png)

### Get all saved claims

- Endpoint path: `/api/saves`
- Endpoint method: `GET`

- Headers:

  - Authorization: Bearer token

- Response: A list of user's saved claims
- Response shape:
  ```json
  {
    [
      {
        "id": int,
        "text": string,
        "claimant": string,
        "date": datetime,
        "textualRating": string,
        "publisher": string,
        "url": string,
        "category_name": string,
        "user_id": int
      }
    ]
  }
  ```

### Create/Save a claim

- Endpoint path: `/api/saves`
- Endpoint method: `POST`

- Headers:

  - Authorization: Bearer token

- Request body:

  ```json
      {
        "text": string,
        "claimant": string,
        "date": datetime,
        "textualRating": string,
        "publisher": string,
        "url": string,
        "category_name": int
      }
  ```

- Response: The claim that was saved with added id and user_id properties
- Response shape:
  ```json
       {
         "id": int,
         "text": string,
         "claimant": string,
         "date": datetime,
         "textualRating": string,
         "publisher": string,
         "url": string,
         "category_name": int,
         "user_id": int
       }
  ```

### Update a saved claim

- Endpoint path: `/api/saves/{save_id}`
- Endpoint method: `PUT`

- Headers:

  - Authorization: Bearer token

- Request shape:

  ```json
      {
          "category_name": string,
      }
  ```

- Response: An indication of success or failure
- Response shape:
  ```json
       {
           "success": true|false
       }
  ```

### Delete a saved claim

- Endpoint path: `/api/saves/{save_id}`
- Endpoint method: `Delete`

- Headers:

  - Authorization: Bearer token

- Response: An indication of success or failure
- Response shape:
  ```json
      {
          "success": true|false
      }
  ```

## Categories Endpoints
![Category Endpoints](./images/categories.png)

### Get all categories

- Endpoint path: `/api/categories`
- Endpoint method: `GET`

- Headers:

  - Authorization: Bearer token

- Response: A list of user's created categories
- Response shape:
  ```json
  {
    [
      {
          "id": int,
          "category_name": string
      }
    ]
  }
  ```

### Create a category

- Endpoint path: `/api/categories`
- Endpoint method: `POST`

- Headers:

  - Authorization: Bearer token

- Request shape:

  ```json
      {
          "category_name": string,
      }
  ```

- Response: The category that was saved with added id property
- Response shape:
  ```json
      {
          "id": int,
          "category_name": string
      }
  ```

### Update a category

- Endpoint path: `/api/categories/{category_id}`
- Endpoint method: `PUT`

- Headers:

  - Authorization: Bearer token

- Request shape:

  ```json
      {
          "name": string,
      }
  ```

- Response: A list of user's saved claims
- Response shape:
  ```json
      {
          "success": true|false
      }
  ```

### Delete a category

- Endpoint path: `/api/categories/{category_id}`
- Endpoint method: `Delete`

- Headers:

  - Authorization: Bearer token

- Response: Dictionary
- Response shape:
  ```json
      {
          "success": true|false
      }
  ```

## Claims Endpoints
![Claims Endpoints](./images/claims.png)

### Get claims from third party

- Endpoint path: `/api/claims`
  -url: (https://factchecktools.googleapis.com/v1alpha1/claims:search?key={SECRET_API_KEY}&languageCode=en-US")
- Endpoint method: `GET`

- Headers:

  - Authorization: SECRET API KEY
  - query: string(claim)

- Response: An array of claims and their matching information (truth, website, publisher, etc)that match the input from the user
- Response shape:

  ```json
  {
    [
      {
          "query": string,
          "claimant":string,
          "date":string,
          "textualRating":string,
          "publisher":string,
          "url":string,

      }
    ]
  }
  ```
