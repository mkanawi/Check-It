import { NavLink } from "react-router-dom";
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";
import useToken from "@galvanize-inc/jwtdown-for-react";

function Nav() {
    const { token } = useAuthContext();
    const { logout } = useToken();

    return (
        <nav className="navbar navbar-expand-lg">
            <NavLink className="navbar-brand mx-2" to="/">
                <img src="./checkitlogoFINAL2.png" className="logo" alt="logo" />
                Checkit
            </NavLink>
            <button
                className="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#navbarNav"
                aria-controls="navbarNav"
                aria-expanded="false"
                aria-label="Toggle navigation"
            >
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNav">
                <ul className="navbar-nav">
                    <li className="nav-item">
                        <NavLink className="nav-link" aria-current="page" to="/">
                            Home
                        </NavLink>
                    </li>
                    {token ? (
                        <>
                            <div className="btn-group" role="group">
                                <NavLink
                                    className="nav-link p-2"
                                    aria-current="page"
                                    to="/saved"
                                >
                                    Your Claims <i className="bi bi-box-arrow-left"></i>
                                </NavLink>
                            </div>
                            <div
                                className="btn-group d-flex justify-content-end"
                                role="group"
                            >
                                <button className="btn btn-outline-secondary" onClick={logout}>
                                    Logout <i className="bi bi-box-arrow-left"></i>
                                </button>
                            </div>
                        </>
                    ) : (
                        <>
                            <li className="nav-item">
                                <NavLink className="nav-link" aria-current="page" to="/signup">
                                    Signup
                                </NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink className="nav-link" aria-current="page" to="/login">
                                    Login
                                </NavLink>
                            </li>
                        </>
                    )}
                </ul>
            </div>
        </nav>
    );
}
export default Nav;
