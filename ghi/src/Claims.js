import "./App.css";
import { NavLink } from "react-router-dom";
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";
import CategoryDropdown from "./CategoryDropdown";
function Claims(props) {
    const { token } = useAuthContext();
    const options = { month: "long", day: "numeric", year: "numeric" };
    const filteredClaims = props.claims.filter((item) => {
        if (props.claim === "") {
            return "";
        } else {
            return item.text.toLowerCase().includes(props.claim.toLowerCase());
        }
    });
    if (filteredClaims.length) {
        return (
            <div className={`container ${props.darkCont}`}>
                <ul>
                    {filteredClaims.map((item) => (
                        <li
                            className={props.smollCont}
                            value={item.text}
                            key={item.claimReview[0].url}
                        >
                            <p>
                                <b>The claim: </b>
                                {item.text}{" "}
                            </p>
                            <div></div>
                            <p>
                                <b>The date:</b>{" "}
                                {item.claimDate ? new Date(item.claimDate).toLocaleDateString("en-US", options) : "Not available"}
                            </p>
                            <div></div>
                            <p>
                                <b>The platform:</b> {item.claimant || "Not available"}
                            </p>
                            <div></div>
                            <p>
                                <b>True or False? </b>
                                {item.claimReview[0].textualRating}
                            </p>
                            <div></div>
                            <p>
                                <b>The reviewer: </b>
                                {item.claimReview[0].publisher.name}
                            </p>
                            <div></div>
                            <p>
                                <b>The website: </b>
                                <a href={item.claimReview[0].url}>{item.claimReview[0].url}</a>
                            </p>
                            {token ? (
                                <CategoryDropdown darkInput={props.darkInput} item={item} />
                            ) : (
                                <NavLink className="nav-link" aria-current="page" to="/signup">
                                    <button className="btn btn-outline-secondary">
                                        Create an account to save
                                    </button>
                                </NavLink>
                            )}
                        </li>
                    ))}
                </ul>
            </div>
        );
    } else {
        return <></>;
    }
}
export default Claims;