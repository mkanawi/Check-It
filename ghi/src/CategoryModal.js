import React, { useState, useEffect } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import { Form } from "react-bootstrap";
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";

function CategoryModal(props) {
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const { token } = useAuthContext();
    const [categories, setCategories] = useState([]);
    const [category, setCategory] = useState("");
    const [newCat, setNewCat] = useState("");

    const handleCategoryChange = (event) => {
        const value = event.target.value;
        setCategory(value);
    };

    const handleNewCatChange = (event) => {
        const value = event.target.value;
        setNewCat(value);
    };

    useEffect(() => {
        const fetchData = async () => {
            const url = `${process.env.REACT_APP_API_HOST}/api/categories`;

            const response = await fetch(url, {
                credentials: "include",
                headers: { Authorization: `${token}` },
            });

            if (response.ok) {
                const data = await response.json();
                setCategories(data);
            }
        };

        fetchData();
    }, [token]);

    const handleSubmit = async (event) => {
        const data = {};
        event.preventDefault();
        if (newCat.length) {
            const postData = { category_name: newCat };
            const catUrl = `${process.env.REACT_APP_API_HOST}/api/categories`;
            const fetchConfig = {
                method: "post",
                body: JSON.stringify(postData),
                credentials: "include",
                headers: {
                    Authorization: `${token}`,
                    "Content-Type": "application/json",
                },
            };
            const response = await fetch(catUrl, fetchConfig);
            if (response.ok) {
                await response.json();
            }
            data.category_name = newCat;
        } else {
            data.category_name = category;
        }

        const updateSavesUrl = `${process.env.REACT_APP_API_HOST}/api/saves/${props.save_id}`;
        const fetchConfig = {
            method: "put",
            body: JSON.stringify(data),
            credentials: "include",
            headers: {
                Authorization: `${token}`,
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(updateSavesUrl, fetchConfig);
        if (response.ok) {
            await response.json();
            setCategory("");
            setShow(false);
            window.location.reload();
        }
    };

    return (
        <>
            <Button
                variant="btn btn-outline-secondary"
                id="change-category"
                onClick={handleShow}
            >
                Change category
            </Button>

            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title id="delete-pop" className="popup-title">
                        Change category
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group className="mb-3">
                            <Form.Select
                                onChange={handleCategoryChange}
                                name="category"
                                required
                                value={category}
                            >
                                <option value="">Choose a category...</option>
                                {categories.map((c) => {
                                    return (
                                        <option key={c.id} value={c.category_name}>
                                            {c.category_name}
                                        </option>
                                    );
                                })}
                            </Form.Select>
                        </Form.Group>
                        <p id="delete-pop">or</p>
                        <Form.Group className="mb-3">
                            <Form.Control
                                id="new-cat"
                                onChange={handleNewCatChange}
                                type="text"
                                placeholder="Make a new category..."
                                value={newCat}
                            />
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="btn btn-outline-secondary" onClick={handleClose}>
                        Close
                    </Button>
                    <Button variant="btn btn-outline-secondary" onClick={handleSubmit}>
                        Save Changes
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

export default CategoryModal;
