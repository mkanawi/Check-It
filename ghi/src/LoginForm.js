import React, { useState, useEffect } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import "./App.css";
import { useNavigate, Navigate } from "react-router-dom";


function LoginForm(props) {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const { token, login } = useToken();
    const navigate = useNavigate();
    const [isLoggedin, setIsloggedin] = useState(false);

    const handleSubmit = async (e) => {
        e.preventDefault();
        login(username, password);
        if (isLoggedin) {
            navigate("/");
        }
    };

    useEffect(() => {
        if (token) setIsloggedin(true);
    }, [token]);

    if (isLoggedin) {
        return <Navigate replace to="/" />;
    } else {
        return (
            <div className={props.darkCont} id="login-container">
                <form onSubmit={(e) => handleSubmit(e)}>
                    <div className="form-group">
                        <label htmlFor="username">Username</label>
                        <input
                            type="text"
                            className={props.darkInput}
                            id="username"
                            aria-describedby="emailHelp"
                            onChange={(e) => setUsername(e.target.value)}
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="password">Password</label>
                        <input
                            type="password"
                            className={props.darkInput}
                            id="password"
                            onChange={(e) => setPassword(e.target.value)}
                        />
                    </div>
                    <button
                        type="submit"
                        id="loginbtn"
                        className="btn btn-outline-secondary"
                    >
                        Submit
                    </button>
                </form>
            </div>
        );
    }
}
export default LoginForm;
