import { useState } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { useNavigate } from "react-router-dom";

const SignupForm = (props) => {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [first, setFirst] = useState("");
    const [last, setLast] = useState("");
    const { register } = useToken();
    const navigate = useNavigate();

    const handleRegistration = async (e) => {
        e.preventDefault();
        const accountData = {
            first_name: first,
            last_name: last,
            username: username,
            password: password,
        };
        register(accountData, `${process.env.REACT_APP_API_HOST}/api/accounts`);

        e.target.reset();
        navigate("/");
    };

    return (
        <div>
            <div className={props.darkCont} id="sign-container">
                <h5 className="card-header">Signup</h5>
                <div className="card-body">
                    <form onSubmit={(e) => handleRegistration(e)}>
                        <div className="mb-3">
                            <label className="form-label">First Name:</label>
                            <input
                                name="first_name"
                                type="text"
                                className={props.darkInput}
                                onChange={(e) => {
                                    setFirst(e.target.value);
                                }}
                            />
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Last Name</label>
                            <input
                                name="last_name"
                                type="text"
                                className={props.darkInput}
                                onChange={(e) => {
                                    setLast(e.target.value);
                                }}
                            />
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Username</label>
                            <input
                                name="username"
                                type="text"
                                className={props.darkInput}
                                onChange={(e) => {
                                    setUsername(e.target.value);
                                }}
                            />
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Password</label>
                            <input
                                name="password"
                                type="password"
                                className={props.darkInput}
                                onChange={(e) => {
                                    setPassword(e.target.value);
                                }}
                            />
                        </div>
                        <div>
                            <input
                                className="btn btn-outline-secondary"
                                type="submit"
                                value="Register"
                            />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default SignupForm;
