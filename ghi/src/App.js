import { BrowserRouter, Route, Routes } from "react-router-dom";
import React, { useState } from "react";
import "./App.css";
import SearchFacts from "./SearchFacts.js";
import Nav from "./Nav.js";
import { AuthProvider } from "@galvanize-inc/jwtdown-for-react";
import SignupForm from "./SignupForm.js";
import LoginForm from "./LoginForm.js";
import Claims from "./Claims";
import CategoryDropdown from "./CategoryDropdown";
import SaveButton from "./SaveButton";
import RecentClaims from "./RecentClaims";
import SaveClaims from "./SaveClaims";
import ErrorPage from "./ErrorPage";
import LightImage from "./images/sun-2-512.png";
import DarkImage from "./images/moon-1-512.png";
import Loading from "./Loading";


function App() {
    const [dark, setDark] = useState("darkmode");
    const [darkCont, setDarkCont] = useState("darkCont");
    const [icon, setIcon] = useState(LightImage)

    const [darkInput, setDarkInput] = useState("form-control")
    const [darkHeader, setDarkHeader] = useState("lightHeader")
    const [smollCont, setSmollCont] = useState("container-claims")

    const changeStyle = () => {
        if (dark === "darkmode") {
            setDark("lightmode");
            setDarkCont("lightCont");
            setDarkInput("lightInput");
            setDarkHeader("darkHeader")
            setSmollCont("lightContSmoll")
            setIcon(DarkImage)
            document.body.style.backgroundColor = "white";
        } else {
            setDark("darkmode");
            setDarkCont("darkCont");
            setDarkInput("form-control");
            setDarkHeader("lightHeader")
            setSmollCont("container-claims")
            setIcon(LightImage)
            document.body.style.backgroundColor = "#282c34";
        }
    };
    const domain = /https:\/\/[^/]+/;
    const basename = process.env.PUBLIC_URL.replace(domain, '');
    return (
        <div>
            <BrowserRouter basename={basename}>
                <AuthProvider baseUrl={`${process.env.REACT_APP_API_HOST}`}>
                    <div className="content">
                        <Nav />
                        <button onClick={changeStyle} id="change-mode" style={{ backgroundImage: `url(${icon})` }} className="btn btn-outline-secondary"></button>
                        <Routes>
                            <Route path="/">
                                <Route index element={<SearchFacts darkHeader={darkHeader} darkInput={darkInput} darkCont={darkCont} smollCont={smollCont} />} />
                            </Route>
                            <Route path="loading">
                                <Route index element={<Loading />} />
                            </Route>
                            <Route path="save">
                                <Route index element={<SaveButton />} />
                            </Route>
                            <Route path="claims">
                                <Route index element={<Claims darkInput={darkInput} />} />
                            </Route>
                            <Route path="recents">
                                <Route index element={<RecentClaims />} />
                            </Route>
                            <Route path="categories">
                                <Route index element={<CategoryDropdown />} />
                            </Route>
                            <Route path="saved">
                                <Route index element={<SaveClaims darkCont={darkCont} smollCont={smollCont} />} />
                            </Route>
                            <Route path="login">
                                <Route index element={<LoginForm dark={dark} darkCont={darkCont} darkInput={darkInput} />} />
                            </Route>
                            <Route path="signup">
                                <Route index element={<SignupForm dark={dark} darkCont={darkCont} darkInput={darkInput} />} />
                            </Route>
                            <Route path="*" element={<ErrorPage />} />
                        </Routes>
                    </div>
                </AuthProvider>
            </BrowserRouter>
        </div>
    );
}

export default App;
