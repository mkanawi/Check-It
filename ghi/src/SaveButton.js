import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";
import React, { useState } from "react";
import "./App.css";

function SaveButton(props) {
    const { token } = useAuthContext();
    const [alert, setAlert] = useState("saved-alert-hidden");

    const save = async (item) => {
        const data = {};
        data.text = item.text;
        if (item.claimant) {
            data.claimant = item.claimant;
        } else {
            data.claimant = "Not available";
        }
        if (item.claimDate) {
            data.date = item.claimDate;
        } else {
            data.date = "Not available";
        }
        data.textualRating = item.claimReview[0].textualRating;
        data.publisher = item.claimReview[0].publisher.name;
        data.url = item.claimReview[0].url;

        if (props.NewCat.length) {
            // CREATE NEW CATEGORY
            const data2 = {};
            data2.category_name = props.NewCat;
            const claimUrl2 = `${process.env.REACT_APP_API_HOST}/api/categories`;
            const fetchConfig2 = {
                method: "post",
                body: JSON.stringify(data2),
                credentials: "include",
                headers: {
                    Authorization: `${token}`,
                    "Content-Type": "application/json",
                },
            };

            const response2 = await fetch(claimUrl2, fetchConfig2);
            if (response2.ok) {
                await response2.json();
            }

            data.category_name = props.NewCat;
        } else {
            data.category_name = props.category;
        }

        // REQUEST TO SAVE A CLAIM
        const claimUrl = `${process.env.REACT_APP_API_HOST}/api/saves`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            credentials: "include",
            headers: {
                Authorization: `${token}`,
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(claimUrl, fetchConfig);
        if (response.ok) {
            const newClaim = await response.json();

            if (newClaim.category_name) setAlert("saved-alert");
        }
    };

    return (
        <>
            <button
                onClick={() => save(props.item)}
                className="btn btn-outline-secondary"
            >
                Save
            </button>
            <p className={alert}>Saved!</p>
        </>
    );
}

export default SaveButton;
