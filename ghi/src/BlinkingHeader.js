import React, { useState, useEffect, useMemo } from "react";
import "./App.css";

const BlinkingHeader = (props) => {
    const [index] = useState(0);
    const [subIndex, setSubIndex] = useState(0);
    const [blink, setBlink] = useState(true);
    const words = useMemo(() => ["See if it's true."], []);

    useEffect(() => {
        if (index === words.length - 1 && subIndex === words[index].length) {
            return;
        }
        if (subIndex === words[index].length + 1 && index !== words.length - 1) {
            return;
        }
        const timeout = setTimeout(() => {
            setSubIndex((prev) => prev + 1);
        }, Math.max(subIndex === words[index].length ? 1000 : 150, parseInt(Math.random() * 350)));

        return () => clearTimeout(timeout);
    }, [subIndex, index, words]);

    useEffect(() => {
        const timeout2 = setTimeout(() => {
            setBlink((prev) => !prev);
        }, 500);
        return () => clearTimeout(timeout2);
    }, [blink]);

    return (
        <h1 className={props.darkHeader}>
            {words[index].substring(0, subIndex)}
            {blink ? "|" : ""}

        </h1>

    )
}

export default BlinkingHeader
